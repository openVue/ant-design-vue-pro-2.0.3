const path = require('path')
const webpack = require('webpack')
const createThemeColorReplacerPlugin = require('./config/plugin.config')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const CompressionWebpackPlugin = require('compression-webpack-plugin')
function resolve(dir) {
  return path.join(__dirname, dir)
}
const isProd = process.env.NODE_ENV === 'production'
// 生产环境和开发环境要区别开,获取插件plugin配置
function getPlugins() {
  let plugins = []
  let productionGzipExtensions = ['html', 'js', 'css']
  plugins.push(new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/))
  // 生产环境配置
  if (isProd) {
    // 代码压缩
    plugins.push(
      new UglifyJsPlugin({
        uglifyOptions: {
          output: {
            comments: false // 去掉注释
          },
          warnings: false,
          // 生产环境自动删除console
          compress: {
            drop_debugger: true,
            drop_console: true,
            pure_funcs: ['console.log']
          }
        },
        sourceMap: false,
        parallel: true
      }),
      // gzip压缩
      new CompressionWebpackPlugin({
        filename: '[path].gz[query]',
        algorithm: 'gzip',
        test: new RegExp('\\.(' + productionGzipExtensions.join('|') + ')$'),
        threshold: 10240, // 只有大小大于该值的资源会被处理 10240
        minRatio: 0.8, // 只有压缩率小于这个值的资源才会被处理
        deleteOriginalAssets: false // 删除原文件
      })
    )
  }
  return plugins
}
// vue.config.js
const vueConfig = {
  configureWebpack: {
    // webpack plugins
    plugins: getPlugins(),
    mode: isProd ? 'production' : 'development', // 方便开发调试
    devtool: isProd ? false : 'source-map',
    //关闭 webpack 的性能提示
    performance: {
      hints: false
    }
  },
  chainWebpack: (config) => {
    config.plugins.delete('prefetch')
    config.resolve.alias.set('@$', resolve('src'))
    const svgRule = config.module.rule('svg')
    svgRule.uses.clear()
    svgRule
      .oneOf('inline')
      .resourceQuery(/inline/)
      .use('vue-svg-icon-loader')
      .loader('vue-svg-icon-loader')
      .end()
      .end()
      .oneOf('external')
      .use('file-loader')
      .loader('file-loader')
      .options({
        name: 'assets/[name].[hash:8].[ext]'
      })
  },

  css: {
    loaderOptions: {
      less: {
        modifyVars: {
          /* less 变量覆盖，用于自定义 ant design 主题 */
          /*
          'primary-color': '#F5222D',
          'link-color': '#F5222D',
          'border-radius-base': '4px',
          */
        },
        javascriptEnabled: true
      }
    },
    sourceMap: isProd ? false : true
  },
  devServer: {
    port: 8000,
    open: true
  },
  // disable source map in production
  productionSourceMap: false,
  lintOnSave: isProd ? false : true,
  outputDir: 'dist',
  // babel-loader no-ignore node_modules/*
  transpileDependencies: [],
  assetsDir: 'static'
}

if (process.env.VUE_APP_PREVIEW === 'true') {
  console.log('VUE_APP_PREVIEW', true)
  // add `ThemeColorReplacer` plugin to webpack plugins
  vueConfig.configureWebpack.plugins.push(createThemeColorReplacerPlugin())
}

module.exports = vueConfig
