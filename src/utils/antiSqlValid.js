/**
 * @description  防sql注入
 * @export validateIllegalCharacter
 * @param {String} str
 */
export function validateIllegalCharacter(str) {
  const reg = /select|update|delete|truncate|join|union|exec|insert|drop|count|'|"|;|>|<|%/gi
  if (reg.test(str)) {
    return str.replace(str, '')
  } else {
    return str
  }
}
