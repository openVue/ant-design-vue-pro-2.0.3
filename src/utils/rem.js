import { throttle } from '@/utils/share'

// 这里给文字大小限制住，最小12px大小
// 监听窗口大小最好加上节流

// 其实我们在pc端只需要适配三种屏幕分辨率就可以了
// 设计一般会给1920*1080的设计稿
// 1、1366 * 768
// 2、1920*1080
// 3、2k+屏幕

// 基准大小
const baseSize = 16
// 设置rem函数,竖向不行，只监听了宽度!!!!!!!
function setRem() {
  // 当前页面宽度相对于1920宽的缩放比例，可根据自己需要修改。
  const scale = document.documentElement.clientWidth / 1920
  // 设置页面根节点字体大小, 字体大小最小为12
  let fontSize = baseSize * Math.min(scale, 2) > 12 ? baseSize * Math.min(scale, 2) : 12
  document.documentElement.style.fontSize = fontSize + 'px'
}
//初始化
setRem()
//改变窗口大小时重新设置 rem,这里最好加上节流
window.onresize = function () {
  // 给setRem函数加上节流
  throttle(setRem, 100)
}
