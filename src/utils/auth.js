const tokenKey = 'Access-Token'
export function setToken(token) {
  localStorage.setItem(tokenKey, token)
}
export function getToken() {
  return localStorage.getItem(tokenKey)
}
export function delToken() {
  localStorage.removeItem(tokenKey)
}
