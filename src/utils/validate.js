/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validUsername(str) {
  const valid_map = ['admin', 'editor']
  return valid_map.indexOf(str.trim()) >= 0
}

/**
 * @description  校验url
 * @param {string} url
 * @returns {Boolean}
 */
export function validURL(url) {
  const reg =
    /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/
  return reg.test(url)
}

/**
 * @description  判断是否为正确的金额
 * @export isPrice
 * @param {*} value
 *
 */
export function isPrice(rule, value, callback) {
  let reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/
  if (value == '' || value == undefined || value == null) {
    callback('请输入金额！')
  } else {
    if (!reg.test(value) && value != '') {
      callback(new Error('请输入正确的金额！'))
    } else {
      callback()
    }
  }
}
/**
 * @description  正整数校验必填
 * @param {*} value
 */
let fixedNumberLength5 = (rule, value, callback) => {
  if (typeof value != 'undefined' && value != null && value != '') {
    if (/^\d{1,5}$/.test(value) == false) {
      return callback(new Error('请输入正整数，且长度不超过5位数字'))
    } else {
      return callback()
    }
  } else {
    return callback()
  }
}

/**
 * @description  百分数（不带百分号）校验必填
 * @param {*} value
 */
export const validatorPercent = (rule, value, callback) => {
  if (typeof value != 'undefined' && value != null && value != '') {
    if (/^\d{1,3}(\.\d{1,4})?$/.test(value) == false || value > 100) {
      return callback(new Error(`${rule.name}不能超过100，且小数点后面不超过4位`))
    } else {
      return callback()
    }
  } else {
    return callback()
  }
}
export const rulesUtil = {
  decimalTenAndTwo: function (trigger = 'change') {
    return {
      message: '小数点前面不超过10位,小数点后面不超过2位!',
      trigger,
      pattern: /^\d{1,10}(\.\d{1,2})?$/
    }
  },
  decimalEightAndTwo: function (trigger = 'change') {
    return {
      message: '小数点前面不超过8位,小数点后面不超过2位!',
      trigger,
      pattern: /^\d{1,8}(\.\d{1,2})?$/
    }
  },
  decimalEightAndFour: function (trigger = 'change') {
    return {
      message: '小数点前面不超过8位,小数点后面不超过4位!',
      trigger,
      pattern: /^\d{1,8}(\.\d{1,4})?$/
    }
  },
  changeForIntegerLength5: function (trigger = 'change') {
    return {
      trigger,
      validator: fixedNumberLength5
    }
  },
  changeForPercent: function (trigger = 'change', name) {
    return {
      name,
      trigger,
      validator: validatorPercent
    }
  }
}

/**
 * @description 手机号校验
 * @param {*} value
 */
export const validatePhone = (rule, value, callback) => {
  const reg = /^1[3,4,5,7,8,9]\d{9}$/
  const flag = reg.test(value)
  if (!value) {
    callback(new Error('请输入手机号'))
  }
  if (!flag) {
    callback(new Error('请输入正确的手机号'))
  } else {
    callback()
  }
}

/**
 * @description 密码校验
 * @param {*} value
 */
export const validatePassF = (rule, value, callback) => {
  let specialPatrn = '`~!@#$%^&*()_\\-+=<>?:"{}|,.\\/;\'\\\\[\\]·~！@#￥%……&*（）——\\-+={}|《》？：“”【】、；‘’，。、' // 特殊字符匹配表达式
  if (value === this.registerData.username) {
    callback(new Error('密码不能和用户名相同！'))
    return
  }
  let regExp = new RegExp('^[\\dA-Za-z' + specialPatrn + ']{8,12}$')
  if (!regExp.test(value)) {
    callback(new Error('请输入8-12位数字、字母或特殊字符！'))
    return
  }
  regExp = new RegExp(
    '(\\d)([A-Za-z' + specialPatrn + '])|([A-Za-z])([\\d' + specialPatrn + '])|([' + specialPatrn + '])([\\dA-Za-z])'
  )
  if (!regExp.test(value)) {
    callback(new Error('至少输入数字、字母或特殊字符中的两种！'))
    return
  }
  callback()
}

/**
 * @description 重复输入密码确认校验
 * @param {*} value
 */
export const vaildPasswordCheck = (rule, value, callback) => {
  const password = this.registerData.password
  if (value === undefined) {
    callback(new Error('请输入密码！'))
  }
  if (value && password && value.trim() !== password.trim()) {
    callback(new Error('两次输入的密码不匹配!'))
  }
  callback()
}

/**
 * @description 前端验证码校验
 * @param {*} value
 */
export const validateValidCode = (rule, value, callback) => {
  if (value.length < 4) {
    callback(new Error('请输入4位验证码'))
  } else if (value !== this.identifyCode) {
    callback(new Error('您输入的验证码不对'))
  } else {
    callback()
  }
}
