import 'core-js/stable'
import 'regenerator-runtime/runtime'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/'
// WARNING: `mockjs` NOT SUPPORT `IE` PLEASE DO NOT USE IN `production` ENV.
import './mock'
import bootstrap from './core/bootstrap'
import './core/lazy_use'
import './permission' // 权限控制
import moment from 'moment' //导入文件
import './directives' // 自定义指令（按钮权限细分）
// import '@/utils/rem' // px转rem适配 Todo!!
import * as filters from '@/filters' // 全局过滤器
Vue.config.productionTip = false

import { axios } from '@/utils/request'
Vue.prototype.$moment = moment //赋值使用
Vue.prototype.$http = axios //赋值使用
// 全局过滤器
Object.keys(filters).forEach((key) => {
  Vue.filter(key, filters[key])
})
new Vue({
  router,
  store,
  created: bootstrap,
  render: (h) => h(App)
}).$mount('#app')
