import Vue from 'vue'
import permission from './permission'
const directives = {
  permission
}

Object.keys(directives).forEach((name) => Vue.directive(name, directives[name]))
