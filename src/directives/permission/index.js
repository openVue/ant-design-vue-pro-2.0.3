import store from '@/store'
/*
  权限指令用法 v-permission=''aad''
  使用方法:参数为按钮权限或者角色权限
*/
function checkPermission(key) {
  const permissionList = store.state.user.btnsPremissionList
  const userRole = store.state.user.roles.map((item) => item.roleCode)
  // const permissionList = ['user:add']
  // const userRole = '系统管理员'
  if (Array.isArray(key)) {
    // 角色组
    if (key.indexOf(userRole) > -1) {
      return true
    }
  } else {
    // 满足其中一个条件即可
    if (permissionList.indexOf(key) > -1 || userRole === key) {
      return true // 有权限
    }
  }
  return false
}

const permission = {
  inserted: function (el, binding) {
    let permission = binding.value // 获取到 v-permission的值
    if (permission) {
      let hasPermission = checkPermission(permission)
      if (!hasPermission) {
        // 没有权限 移除Dom元素
        el.parentNode && el.parentNode.removeChild(el)
      }
    }
  }
}

export default permission
