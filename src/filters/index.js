import moment from 'moment'
import 'moment/locale/zh-cn'
moment.locale('zh-cn')
/**
 * @description 额千分位格式化
 * @export  amountFilter
 * @param num
 * @returns {string}
 */
const amountFilter = (num) => {
  // 判断传进来的数字是否为非空数字
  if (!isNaN(parseFloat(num))) {
    const reg = /\./g
    const newNum = Number(Number(num).toFixed(2)).toLocaleString()
    // 判断转换后的数字是否带有小数
    if (reg.test(newNum)) {
      const numArr = newNum.split('.')
      // 判断小数点后数字长度为1，则自动补0
      numArr[1] = numArr[1].length === 1 ? numArr[1] + '0' : numArr[1]
      return numArr.join('.')
    } else {
      // 整数直接在后面补上0.00
      return newNum + '.00'
    }
  } else {
    return ''
  }
}
/**
 * @description 数量6位小数
 * @export  quantityFilter
 * @param num
 * @returns {string}
 */
const quantityFilter = (num) => {
  if (!isNaN(parseFloat(num))) {
    const reg = /\./g
    const newNum = Number(Number(num).toFixed(6)).toString()
    // 判断转换后的数字是否带有小数
    if (reg.test(newNum)) {
      const numArr = newNum.split('.')

      // 判断小数点后数字长度为1，则自动补0
      numArr[1] = numArr[1].length === 1 ? numArr[1] + '00000' : numArr[1]
      numArr[1] = numArr[1].length === 2 ? numArr[1] + '0000' : numArr[1]
      numArr[1] = numArr[1].length === 3 ? numArr[1] + '000' : numArr[1]
      numArr[1] = numArr[1].length === 4 ? numArr[1] + '00' : numArr[1]
      numArr[1] = numArr[1].length === 5 ? numArr[1] + '0' : numArr[1]
      return numArr.join('.')
    } else {
      // 整数直接在后面补上0.000000
      return newNum + '.000000'
    }
  } else {
    return ''
  }
}
/**
 * @export  formatDate
 * @description 时间格式化
 * @param {string} date
 * @param {string} [format='YYYY-MM-DD HH:mm:ss']
 * @return {string}
 */
const formatDate = (date, format = 'YYYY-MM-DD HH:mm:ss') => {
  if (date) {
    return moment(date).format(format)
  } else {
    return ''
  }
}
/**
 * @export  NumberFormat
 * @param {string} value
 * @description 将整数部分逢三一断
 * @param {string}
 * @return {string}
 */
const NumberFormat = (value) => {
  if (!value) {
    return '0'
  }
  const intPartFormat = value.toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,') // 将整数部分逢三一断
  return intPartFormat
}

export { amountFilter, quantityFilter, formatDate, NumberFormat }
